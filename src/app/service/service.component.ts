import {Component, OnInit} from "@angular/core";
import {Service} from "../models/service";
import {ServiceService} from "./service.service";
import {Specialty} from "../models/specialty";
import {HttpErrorResponse} from "@angular/common/http";
import {ADAService} from "../storage/data-access-service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MyErrorHandler} from "../handler/my-error-handler.service";
import {User} from "../models/user";
import {DTService} from "../storage/data-transfer-service";
import {Router} from "@angular/router";
import {AuthService} from "../auth/auth.service";


@Component({
  selector: 'service-app',
  templateUrl: './service.component.html'
})
export class ServiceComponent implements OnInit{

  page: number = 1;
  public services: Service[] ;
  specialties: Specialty[];
  role: string;

  errorMessage: string;
  errorOnForm: boolean = false;

  editService: Service;
  deleteService: Service;

  serviceForm: FormGroup;

  constructor(public formBuilder: FormBuilder,
              private serviceService: ServiceService,
              private loginService: AuthService,
              private adaService: ADAService,
              private dtService: DTService,
              private router: Router,
              private myErrorHandler: MyErrorHandler) {
    this.serviceForm = this.formBuilder.group({
      name: [''],
      specialty: [''],
      price: ['']
    });
  }

  ngOnInit(): void {
    if (localStorage.getItem('username') != null) {
      this.adaService.getUserByUsername(localStorage.getItem('username')).subscribe(
        (response: User) => {
          this.role = response.userRole;
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.role = 'guest';
    }
    this.getServices();
    this.getSpecialties();

    this.serviceForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.pattern('^[A-Z][a-z]*$'), Validators.minLength(3), Validators.maxLength(30)]),
      specialty: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required, Validators.pattern('^[1-9][0-9]*$'), Validators.min(100)])
    });
  }

  getSpecialties(): void {
    this.adaService.getSpecialties().subscribe(
      (res) => {
        this.specialties = res;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  public getServices(): void {
    this.serviceService.getServices().subscribe(
      (response: Service[]) => {
        this.services = response;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  doSomeOperation(service: Service, operation: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if (service !== null) {
      this.serviceService.getServiceById(service.id).subscribe(
        (response: Service) => {
          if (operation === 'edit') {
            this.patchServiceForm(service);
            button.setAttribute('data-target', '#updateServiceModal');
          }

          if (operation === 'delete') {
            this.deleteService = service;
            button.setAttribute('data-target', '#deleteServiceModal');
          }
          container.appendChild(button);
          button.click();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
          this.getServices();
        }
      );
    } else {
      if (operation === 'add') {
        this.serviceForm.reset();
        this.errorOnForm = false;
        button.setAttribute('data-target', '#addServiceModal');
      }
      if (operation === 'showError') {
        button.setAttribute('data-target', '#errorModal');
      }

      container.appendChild(button);
      button.click();
    }

  }


  onAddService() {

    if (this.serviceForm.valid && this.serviceForm.value.specialty !== 'choose...') {
      this.errorOnForm = false;
      document.getElementById('add-service-form').click();
      this.serviceService.createService(parseInt(this.serviceForm.value.specialty), this.serviceForm).subscribe(
        (response:any) => {
          this.getServices();
          this.page = 1;
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.errorOnForm = true;
    }
  }

  onUpdateService() {

    if (this.serviceForm.valid) {
      this.errorOnForm = false;
      document.getElementById('edit-service-form').click();
      this.serviceService.updateService(this.editService.id, parseInt(this.serviceForm.value.specialty), this.serviceForm).subscribe(
        (response:any) => {
          this.getServices();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.errorOnForm = true;
    }
  }

  patchServiceForm(service: Service) {
    this.errorOnForm = false;
    this.editService = service;
    this.serviceForm.reset();
    this.serviceForm.patchValue({
      name: service.name,
      specialty: service.specialty.id,
      price: service.price
    })
    console.log(this.serviceForm.value);
  }

  onDeleteService(serviceId: number) {

    this.serviceService.deleteService(serviceId).subscribe(
      (res:any) => {
        this.getServices();
        this.page = 1;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  addTreatmentWithService(service: Service) {
      if (this.role !== 'guest') {
        this.dtService.changeService(service);
        this.dtService.service = service;
        this.router.navigate(['/treatments/all']);
      } else {
        this.router.navigate(['/users/login']);
      }
  }

  handleErrorResponse(error: HttpErrorResponse) {
    this.errorMessage = this.myErrorHandler.handleError(error);
    this.doSomeOperation(null, 'showError');
    this.page = 1;
  }
}
