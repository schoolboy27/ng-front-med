import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Service} from "../models/service";
import {FormGroup} from "@angular/forms";


@Injectable()
export class ServiceService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {
  }

  getServices() : Observable<Service[]> {
    return this.http.get<Service[]>(`${this.apiServerUrl}/centerservices/all`);
  }

  getServiceById(serviceId: number) {
    return this.http.get(`${this.apiServerUrl}/centerservices/${serviceId}`);
  }

  getServicesBySpecialtyId(specialtyId: number): Observable<Service[]> {
    return this.http.get<Service[]>(`${this.apiServerUrl}/centerservices/spec/${specialtyId}`);
  }

  createService(specialtyId: number, service: FormGroup) {
    return this.http.post(`${this.apiServerUrl}/centerservices/create/specialty/${specialtyId}`,
      {'name': service.value.name, 'price': service.value.price});
  }

  updateService(serviceId: number, specialtyId: number, body: any) {
    return this.http.post(`${this.apiServerUrl}/centerservices/update/${serviceId}/spec/${specialtyId}`,
      {'name': body.value.name, 'price': body.value.price });
  }

  deleteService(serviceId: number) {
    return this.http.delete(`${this.apiServerUrl}/centerservices/delete/${serviceId}`);
  }


}
