import {Component, OnInit} from "@angular/core";
import {Doctor} from "../models/doctor";
import {DoctorService} from "./doctor.service";
import {HttpErrorResponse} from "@angular/common/http";
import {User} from "../models/user";
import {ADAService} from "../storage/data-access-service";
import {Specialty} from "../models/specialty";
import {Center} from "../models/center";
import {NgForm} from "@angular/forms";
import {MyErrorHandler} from "../handler/my-error-handler.service";
import {DTService} from "../storage/data-transfer-service";
import {UserService} from "../user/user.service";
import {DoctorDto} from "../models/doctor.dto";
import {Router} from "@angular/router";
import {UserDto} from "../models/user.dto";


@Component({
  selector: 'doc-app',
  templateUrl: './doctor.component.html'
})
export class DoctorComponent implements OnInit{
  page: number = 1;

  errorMessage: string;
  errorOnForm: boolean = false;

  public doctors: Doctor[] | undefined;
  doctorsDto: DoctorDto[];

  possibleDoctors: UserDto[];
  specialties: Specialty[];
  centers: Center[];

  editDoctor: Doctor;
  deleteDoctor: Doctor;

  role: string;


  constructor(private doctorService: DoctorService,
              private adaService: ADAService,
              private userService: UserService,
              private myErrorHandler: MyErrorHandler,
              private router: Router,
              private dtService: DTService) {
  }

  ngOnInit(): void {

    if(localStorage.getItem('username') !== null) {
      this.adaService.getUserByUsername(localStorage.getItem('username')).subscribe(
        (response: User) => {
          this.role = response.userRole;
          if (response.userRole === 'ROLE_ADMIN') {
            this.getDoctors();
            this.getPossibleDoctors();
          }
          else {
            this.getDoctorsForUser();
          }
        }
      );
    } else {
      this.role = 'guest';
      this.getDoctorsForUser();
    }

    this.getSpecialties();
    this.getCenters();
  }

  getDoctorsForUser() {
    this.doctorService.getDoctorsForUser().subscribe(
      (response: DoctorDto[]) => {
        this.doctorsDto = response;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  public getDoctors(): void {
    this.doctorService.getDoctors().subscribe(
      (response: Doctor[]) => {
        console.log(response);
        this.doctors = response;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  public getPossibleDoctors() {
    this.userService.getUsersNotDoctors().subscribe(
      (res:UserDto[]) => {
        this.possibleDoctors = res;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  public getSpecialties() {
    this.adaService.getSpecialties().subscribe(
      (res) => {
        this.specialties = res;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  public getCenters() {
    this.adaService.getCenters().subscribe(
      (res) => {
        this.centers = res;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      });
  }


  doSomeOperation(doctor: Doctor, operation: string) {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if (doctor !== null) {
      this.userService.getUserById(doctor.user.id).subscribe(
        (response: User) => {
          if (operation === 'edit') {
            this.errorOnForm = false;
            this.editDoctor = doctor;
            button.setAttribute('data-target', '#updateDoctorModal');
          }

          if (operation === 'delete') {
            this.deleteDoctor = doctor;
            button.setAttribute('data-target', '#deleteDoctorModal');
          }
          container.appendChild(button);
          button.click();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
          this.getDoctors();
        }
      );
    } else {
      if (operation === 'add') {
        this.errorOnForm = false;
        button.setAttribute('data-target', '#addDoctorModal');
      }
      if (operation === 'showError') {
        button.setAttribute('data-target', '#errorModal');
      }
      container.appendChild(button);
      button.click();
    }

  }

  onAddDoctor(addForm: NgForm) {

    if (addForm.valid) {

      this.errorOnForm = false;

      this.doctorService.createDoctor(addForm).subscribe(
        (res: any) => {
          document.getElementById('add-doctor-form').click();
          this.getDoctors();
          this.page = 1;
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.errorOnForm = true;
    }


  }


  onUpdateDoctor(doctorId: number, editForm: NgForm) {

    if (editForm.valid) {

      this.errorOnForm = false;
      this.doctorService.updateDoctor(doctorId, editForm).subscribe(
        (res: any) => {
          document.getElementById('edit-doctor-form').click();
          this.getDoctors();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.errorOnForm = true;
    }
  }


  onDeleteDoctor(doctorId: number) {
    this.doctorService.deleteDoctor(doctorId).subscribe(
      (res: any) => {
        this.getDoctors();
        this.page = 1;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  addTreatmentWithDoctor(doctor: DoctorDto) {

    if (this.role !== 'guest') {
      this.dtService.changeDoctor(doctor);
      this.dtService.doctor = doctor;
      this.router.navigate(['/treatments/all']);
    } else {
      this.router.navigate(['/users/login']);
    }

  }

  handleErrorResponse(error: HttpErrorResponse) {
    this.errorMessage = this.myErrorHandler.handleError(error);
    this.doSomeOperation(null, 'showError');
    this.page = 1;
  }
}
