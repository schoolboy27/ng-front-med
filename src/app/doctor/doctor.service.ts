import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Doctor} from "../models/doctor";
import {NgForm} from "@angular/forms";
import {DoctorDto} from "../models/doctor.dto";


@Injectable()
export class DoctorService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {
  }

  getDoctors(): Observable<Doctor[]> {
    return this.http.get<Doctor[]>(`${this.apiServerUrl}/doctors/all`)
  }

  getDoctorsBySpecialtyId(specialtyId: number): Observable<DoctorDto[]> {
    return this.http.get<DoctorDto[]>(`${this.apiServerUrl}/doctors/spec/${specialtyId}`);
  }


  getDoctorsForUser(): Observable<DoctorDto[]> {
    return this.http.get<DoctorDto[]>(`${this.apiServerUrl}/doctors/all/to/user`);
  }

  createDoctor(addForm: NgForm) {

    let userId = parseInt(addForm.value.user);
    let specId = parseInt(addForm.value.specialty);
    let centerId = parseInt(addForm.value.center);

    return this.http.post(`${this.apiServerUrl}/doctors/create/user/${userId}/spec/${specId}/cntr/${centerId}`,
      {
        'salary': addForm.value.salary
      });
  }

  updateDoctor(doctorId: number, editForm: NgForm) {

    let centerId = parseInt(editForm.value.centerd);
    let specId = parseInt(editForm.value.specialty);

    return this.http.post(`${this.apiServerUrl}/doctors/update/doc/${doctorId}/cntr/${centerId}/spec/${specId}`,
      {
        'salary': editForm.value.salary
      });
  }

  deleteDoctor(doctorId: number) {
    return this.http.delete(`${this.apiServerUrl}/doctors/delete/${doctorId}`);
  }


}

