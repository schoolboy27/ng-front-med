import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {MyLocation} from "../models/mylocation";


@Injectable()
export class LocationService {

  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http:HttpClient) {
  }

  public getLocations(): Observable<MyLocation[]> {
    return this.http.get<MyLocation[]>(`${this.apiServerUrl}/locations/all`);
  }

  getLocationById(locationId: number) {
    return this.http.get(`${this.apiServerUrl}/locations/${locationId}`);
  }

  getFreeLocations(): Observable<MyLocation[]> {
    return this.http.get<MyLocation[]>(`${this.apiServerUrl}/locations/free`);
  }

  createLocation(location: any) {
    return this.http.post(`${this.apiServerUrl}/locations/create`, location);
  }

  updateLocation(locationId: number, location: any) {
    return this.http.post(`${this.apiServerUrl}/locations/update/${locationId}`, location);
  }

  deleteLocation(locationId: number) {
    return this.http.delete(`${this.apiServerUrl}/locations/delete/${locationId}`);
  }

}
