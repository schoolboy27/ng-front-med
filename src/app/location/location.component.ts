import {Component, OnInit} from "@angular/core";
import {MyLocation} from "../models/mylocation";
import {LocationService} from "./location.service";
import {HttpErrorResponse} from "@angular/common/http";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MyErrorHandler} from "../handler/my-error-handler.service";
import {UserService} from "../user/user.service";
import {Router} from "@angular/router";
import {User} from "../models/user";


@Component({
  selector: 'loc-app',
  templateUrl: './location.component.html'
})
export class LocationComponent implements OnInit{

  page: number = 1;
  locations: MyLocation[] | undefined;

  locationForm: FormGroup;

  editLocation: MyLocation;
  deleteLocation: MyLocation;

  errorMessage: string;
  errorOnForm: boolean = false;

  role: string;

  constructor(private locationService: LocationService,
              private myErrorHandler: MyErrorHandler,
              private userService: UserService,
              private router: Router,
              private formBuilder: FormBuilder) {
    this.locationForm = this.formBuilder.group({
      street: '',
      building: '',
      apartment: ''
    });
  }

  ngOnInit(): void {

    if (localStorage.getItem('username') !== null) {
      this.userService.getUserByUsername(localStorage.getItem('username')).subscribe(
        (response:User) => {
          this.role = response.userRole;
          if (this.role === 'ROLE_ADMIN') {
            this.getLocations();
          }
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.router.navigate(['/users/login']);
    }

    this.locationForm = new FormGroup({
      street: new FormControl('', [Validators.required, Validators.maxLength(30), Validators.minLength(3)]),
      building: new FormControl('', [Validators.required, Validators.pattern('^[1-9][0-9]{0,2}[a-z]?$')]),
      apartment: new FormControl('', [Validators.required, Validators.minLength(1)]),
    });
  }

  public getLocations(): void {
    this.locationService.getLocations().subscribe(
      (response: MyLocation[]) => {
        this.locations = response
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    )
  }

    doSomeOperation(location: MyLocation, operation: string) {
      const container = document.getElementById('main-container');
      const button = document.createElement('button');
      button.type = 'button';
      button.style.display = 'none';
      button.setAttribute('data-toggle', 'modal');

      if (location !== null) {
        this.locationService.getLocationById(location.id).subscribe(
          (response: Location) => {
            if (operation === 'edit') {
              this.patchValues(location);
              button.setAttribute('data-target', '#updateLocationModal');
            }

            if (operation === 'delete') {
              this.deleteLocation = location;
              button.setAttribute('data-target', '#deleteLocationModal');
            }
            container.appendChild(button);
            button.click();
          },
          (error: HttpErrorResponse) => {
            this.handleErrorResponse(error);
            this.getLocations();
          }
        );
      } else {
        if (operation === 'add') {
          this.locationForm.reset();
          this.errorOnForm = false;
          button.setAttribute('data-target', '#addLocationModal');
        }
        if (operation === 'showError') {
          button.setAttribute('data-target', '#errorModal');
        }
        container.appendChild(button);
        button.click();
      }
    }


  onAddLocation() {
    if (this.locationForm.valid) {
      this.errorOnForm = false;
      document.getElementById('add-location-form').click();
      this.locationService.createLocation(this.locationForm.value).subscribe(
        (res:any) => {
          this.getLocations();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.errorOnForm = true;
    }
  }

  onUpdateLocation() {

    if (this.locationForm.valid) {
      this.errorOnForm = false;
      document.getElementById('edit-location-form').click();
      this.locationService.updateLocation(this.editLocation.id, this.locationForm.value).subscribe(
        (res:any) => {
          this.getLocations();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.errorOnForm = true;
    }
  }

  onDeleteLocation(locationId: number) {

    this.locationService.deleteLocation(locationId).subscribe(
      (res:any) => {
        this.getLocations();
        this.page = 1;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  patchValues(location: MyLocation) {
    this.errorOnForm = false;
    this.editLocation = location;
    this.locationForm.reset();
    this.locationForm.patchValue({
      street: location.street,
      building: location.building,
      apartment: location.apartment,
    });
  }

  handleErrorResponse(error: HttpErrorResponse) {
    this.errorMessage = this.myErrorHandler.handleError(error);
    this.doSomeOperation(null, 'showError');
    this.page = 1;
  }
}
