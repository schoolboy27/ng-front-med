import {Specialty} from "./specialty";

export class Service {

  constructor(private _id: number,
              private _name: string,
              private _specialty: Specialty,
              private _price: number) {
  }


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get specialty(): Specialty {
    return this._specialty;
  }

  set specialty(value: Specialty) {
    this._specialty = value;
  }

  get price(): number {
    return this._price;
  }

  set price(value: number) {
    this._price = value;
  }
}
