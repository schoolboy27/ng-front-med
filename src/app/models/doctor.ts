import {User} from "./user";
import {Specialty} from "./specialty";
import {Center} from "./center";

export class Doctor {

  constructor(
              private _user: User,
              private _specialty: Specialty,
              private _salary: number,
              private _center: Center) {
  }

  get user(): User {
    return this._user;
  }

  set user(value: User) {
    this._user = value;
  }

  get specialty(): Specialty {
    return this._specialty;
  }

  set specialty(value: Specialty) {
    this._specialty = value;
  }

  get salary(): number {
    return this._salary;
  }

  set salary(value: number) {
    this._salary = value;
  }

  get center(): Center {
    return this._center;
  }

  set center(value: Center) {
    this._center = value;
  }
}
