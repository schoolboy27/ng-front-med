export class UserDto {

  constructor(private _id: number,
              private _lastName: string,
              private _firstName: string,
              private _middleName: string,
              private _role: string
              ) {}


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get middleName(): string {
    return this._middleName;
  }

  set middleName(value: string) {
    this._middleName = value;
  }

  get role(): string {
    return this._role;
  }

  set role(value: string) {
    this._role = value;
  }

  toMyString(): string {
    return this.id + ' ' + this.lastName
      + ' ' + this.firstName
      + ' ' + this.middleName
      + ', ' + this.role;
  }

}
