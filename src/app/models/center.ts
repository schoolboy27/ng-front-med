import {User} from "./user";
import {MyLocation} from "./mylocation";

export class Center {

  constructor(private _id: number,
              private _location: MyLocation,
              private _users: User[]) {
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }


  get location(): MyLocation {
    return this._location;
  }

  set location(value: MyLocation) {
    this._location = value;
  }

  get users(): User[] {
    return this._users;
  }

  set users(value: User[]) {
    this._users = value;
  }
}
