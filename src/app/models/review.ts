import {Doctor} from "./doctor";
import {User} from "./user";

export class Review {
  constructor(private _id: number,
              private _doctor: Doctor,
              private _patient: User,
              private _reviewBody: string) {
  }


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get doctor(): Doctor {
    return this._doctor;
  }

  set doctor(value: Doctor) {
    this._doctor = value;
  }

  get patient(): User {
    return this._patient;
  }

  set patient(value: User) {
    this._patient = value;
  }

  get reviewBody(): string {
    return this._reviewBody;
  }

  set reviewBody(value: string) {
    this._reviewBody = value;
  }
}
