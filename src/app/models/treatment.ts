import {Service} from "./service";
import {Doctor} from "./doctor";
import {User} from "./user";

export class Treatment {
  constructor(private _id: number,
              private _service: Service,
              private _doctor: Doctor,
              private _patient: User,
              private _appointment: Date
  ) {
  }


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get service(): Service {
    return this._service;
  }

  set service(value: Service) {
    this._service = value;
  }

  get doctor(): Doctor {
    return this._doctor;
  }

  set doctor(value: Doctor) {
    this._doctor = value;
  }

  get patient(): User {
    return this._patient;
  }

  set patient(value: User) {
    this._patient = value;
  }


  get appointment(): Date {
    return this._appointment;
  }

  set appointment(value: Date) {
    this._appointment = value;
  }
}
