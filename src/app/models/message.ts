import {UserDto} from "./user.dto";

export class Message {

  constructor(private _id: number,
              private _sender: UserDto,
              private _recipient: UserDto,
              private _topic: string,
              private _messageBody: string,
              private _messageTime: Date) {
  }


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }


  get sender(): UserDto {
    return this._sender;
  }

  set sender(value: UserDto) {
    this._sender = value;
  }

  get recipient(): UserDto {
    return this._recipient;
  }

  set recipient(value: UserDto) {
    this._recipient = value;
  }

  get topic(): string {
    return this._topic;
  }

  set topic(value: string) {
    this._topic = value;
  }


  get messageBody(): string {
    return this._messageBody;
  }

  set messageBody(value: string) {
    this._messageBody = value;
  }


  get messageTime(): Date {
    return this._messageTime;
  }

  set messageTime(value: Date) {
    this._messageTime = value;
  }
}
