export class MyLocation {

  constructor(private _id: number, private _street: string, private _building: string, private _apartment: number) {
  }


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get street(): string {
    return this._street;
  }

  set street(value: string) {
    this._street = value;
  }

  get building(): string {
    return this._building;
  }

  set building(value: string) {
    this._building = value;
  }

  get apartment(): number {
    return this._apartment;
  }

  set apartment(value: number) {
    this._apartment = value;
  }
}
