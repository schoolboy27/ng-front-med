export class ReviewDto {

  constructor(private _id: number,
              private _doctorLastName: string,
              private _doctorFirstName: string,
              private _doctorMiddleName: string,
              private _specialty: string,
              private _userFirstName: string,
              private _reviewBody: string
              ) {}


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get doctorLastName(): string {
    return this._doctorLastName;
  }

  set doctorLastName(value: string) {
    this._doctorLastName = value;
  }

  get doctorFirstName(): string {
    return this._doctorFirstName;
  }

  set doctorFirstName(value: string) {
    this._doctorFirstName = value;
  }

  get doctorMiddleName(): string {
    return this._doctorMiddleName;
  }

  set doctorMiddleName(value: string) {
    this._doctorMiddleName = value;
  }

  get specialty(): string {
    return this._specialty;
  }

  set specialty(value: string) {
    this._specialty = value;
  }

  get userFirstName(): string {
    return this._userFirstName;
  }

  set userFirstName(value: string) {
    this._userFirstName = value;
  }

  get reviewBody(): string {
    return this._reviewBody;
  }

  set reviewBody(value: string) {
    this._reviewBody = value;
  }
}
