import {Specialty} from "./specialty";

export class DoctorDto {

  constructor(private _id: number,
              private _lastName: string,
              private _firstName: string,
              private _middleName: string,
              private _specialty: Specialty
              ) {}

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get middleName(): string {
    return this._middleName;
  }

  set middleName(value: string) {
    this._middleName = value;
  }

  get specialty(): Specialty {
    return this._specialty;
  }

  set specialty(value: Specialty) {
    this._specialty = value;
  }
}
