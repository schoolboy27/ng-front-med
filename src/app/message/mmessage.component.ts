import {Component, OnInit} from "@angular/core";
import {Message} from "../models/message";
import {MmessageService} from "./mmessage.service";
import {HttpErrorResponse} from "@angular/common/http";
import {User} from "../models/user";
import {ADAService} from "../storage/data-access-service";
import {NgForm} from "@angular/forms";
import {MyErrorHandler} from "../handler/my-error-handler.service";
import {Router} from "@angular/router";

@Component({
  selector: 'mess-app',
  templateUrl: './mmessage.component.html'
})
export class MmessageComponent implements OnInit{

  page: number = 1;
  public messages: Message[] | undefined;
  senders: User[];
  recipients: User[];

  errorMessage: string;
  errorOnForm: boolean = false;

  editMessage: Message;
  deleteMessage: Message;

  role: string;

  constructor(private mmessageService: MmessageService,
              private adaService: ADAService,
              private myErrorHandler: MyErrorHandler,
              private router: Router) {
  }

  ngOnInit(): void {

    if (localStorage.getItem('username') !== null) {
      this.adaService.getUserByUsername(localStorage.getItem('username')).subscribe(
        (response: User) => {
          this.role = response.userRole;
          if (this.role === 'ROLE_ADMIN') {
            this.getMessages();
            this.getSendersAndRecipients();
          }
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.router.navigate(['/users/login']);
    }
  }

  public getMessages(): void {
    this.mmessageService.getMessages().subscribe(
      (response: Message[]) => {
        this.messages = response;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  getSendersAndRecipients() {
    this.adaService.getUsers().subscribe(
      (res) => {
        this.senders = res;
        this.recipients = res;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  doSomeOperation(message: Message, operation: string) {

    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if (message !== null) {
      this.mmessageService.getMessageById(message.id).subscribe(
        (response: Message) => {
          if (operation === 'edit') {
            this.errorOnForm = false;
            this.editMessage = message;
            button.setAttribute('data-target', '#updateMessageModal');
          }

          if (operation === 'delete') {
            this.deleteMessage = message;
            button.setAttribute('data-target', '#deleteMessageModal');
          }
          container.appendChild(button);
          button.click();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
          this.getMessages();
        }
      );
    } else {
      if (operation === 'add') {
        this.errorOnForm = false;
        button.setAttribute('data-target', '#addMessageModal');
      }
      if (operation === 'showError') {
        button.setAttribute('data-target', '#errorModal');
      }
      container.appendChild(button);
      button.click();
    }

  }


  onAddMessage(addForm: NgForm) {

    if (addForm.valid) {
      this.errorOnForm = false;
      document.getElementById('add-message-form').click();
      this.mmessageService.createMessage(
        parseInt(addForm.value.sender),
        parseInt(addForm.value.recipient),
        addForm).subscribe(
        (res: any) => {
          this.getMessages();
          this.page = 1;
        }
      );
    } else {
      this.errorOnForm = true;
    }



  }

  onUpdateMessage(messageId: number, editForm: NgForm) {

    console.log(editForm.value)
    if (editForm.valid) {
      this.errorOnForm = false;
      document.getElementById('edit-message-form').click();
      this.mmessageService.updateMessage(messageId, editForm).subscribe(
        (res: any) => {
          this.getMessages();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.errorOnForm = true;
    }

  }


  onDeleteMessage(messageId: number) {
    this.mmessageService.deleteReview(messageId).subscribe(
      (res:any) => {
        this.getMessages();
        this.page = 1;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  handleErrorResponse(error: HttpErrorResponse) {
    this.errorMessage = this.myErrorHandler.handleError(error);
    this.doSomeOperation(null, 'showError');
    this.page = 1;
  }
}
