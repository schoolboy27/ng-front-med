import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Message} from "../models/message";
import {NgForm} from "@angular/forms";


@Injectable()
export class MmessageService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {
  }

  getMessages(): Observable<Message[]> {
    return this.http.get<Message[]>(`${this.apiServerUrl}/messages/all`);
  }

  getMessageById(messageId: number) {
    return this.http.get(`${this.apiServerUrl}/messages/${messageId}`);
  }

  getMessagesByTwoUsers(userOneId: number, userTwoId: number): Observable<Message[]> {
    return this.http.get<Message[]>(`${this.apiServerUrl}/messages/find/by/users/${userOneId}/${userTwoId}`);
  }


  createMessage(senderId: number, recipientId: number, addForm: any) {

    return this.http.post(`${this.apiServerUrl}/messages/create/from/${senderId}/to/${recipientId}`,
      {
        'topic': addForm.value.topic,
        'messageBody': addForm.value.message,
        'messageTime': new Date()
      }
    );
  }

  updateMessage(messageId: number, editForm: NgForm) {

    return this.http.post(`${this.apiServerUrl}/messages/update/${messageId}`,
      {
        'topic': editForm.value.topic,
        'messageBody': editForm.value.message,
        'messageTime': new Date()
      });
  }

  deleteReview(messageId: number) {
    return this.http.delete(`${this.apiServerUrl}/messages/delete/${messageId}`);
  }


}
