import {Component} from "@angular/core";
import {Router} from "@angular/router";


@Component({
  selector: 'err-unauthor',
  templateUrl: './unauthorized-error.component.html'
})
export class UnauthorizedErrorComponent {

  constructor(private router: Router) {
  }

  doLogin() {
    this.router.navigate(['/users/login']);
  }

  doMain() {
    this.router.navigate(['/']);
  }
}
