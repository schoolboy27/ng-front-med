import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Treatment} from "../models/treatment";
import {DoctorDto} from "../models/doctor.dto";
import {UserDto} from "../models/user.dto";

@Injectable()
export class TreatmentService {

  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {
  }

  getTreatments(): Observable<Treatment[]> {
    return this.http.get<Treatment[]>(`${this.apiServerUrl}/treatments/all`)
  }

  getTreatmentById(treatmentId: number) {
    return this.http.get(`${this.apiServerUrl}/treatments/${treatmentId}`);
  }

  getTreatmentsByPatientId(patientId: number) {
    return this.http.get(`${this.apiServerUrl}/treatments/find/patient/${patientId}`);
  }

  getTreatmentsByDoctorId(doctorId: number) {
    return this.http.get(`${this.apiServerUrl}/treatments/find/doctor/${doctorId}`);
  }

  getDoctorsByPatientId(patientId: number): Observable<UserDto[]>{
    return this.http.get<UserDto[]>(`${this.apiServerUrl}/treatments/find/doctors/by/patient/${patientId}`);
  }

  getDoctorInterlocutors(doctorId: number): Observable<UserDto[]> {
    return this.http.get<UserDto[]>(`${this.apiServerUrl}/treatments/find/interlocutors/by/doctor/${doctorId}`);
  }

  updateTreatment(treatmentId: number, serviceId: number, doctorId: number, patientId: number, appointment: string) {
    return this.http.post<any>(`${this.apiServerUrl}/treatments/update/${treatmentId}/serv/${serviceId}/doc/${doctorId}/pat/${patientId}`, {
      'appointment': appointment
    });

  }

  createTreatment(serviceId: number, doctorId: number, patientId: number, appointment: string) {

    return this.http.post<any>(`${this.apiServerUrl}/treatments/create/service/${serviceId}/doc/${doctorId}/patient/${patientId}`,
      {
        'appointment' : appointment});
  }

  deleteTreatment(treatmentId: number): Observable<Treatment> {
    return this.http.delete<Treatment>(`${this.apiServerUrl}/treatments/delete/${treatmentId}`);
  }

  getDoctorFreeTime(doctorId: number, date: string) {
    return this.http.post<any>(`${this.apiServerUrl}/schedule/doc/${doctorId}`, date);
  }

  updateDoctorSchedule(doctorId: number, patientId: number, appointment: string, oldAppointment: string) {
    console.log(appointment);
    return this.http.post(`${this.apiServerUrl}/schedule/update`, {
      'doctor': doctorId,
      'patient': patientId,
      'appointment': appointment,
      'oldAppointment': oldAppointment
    });
  }


}
