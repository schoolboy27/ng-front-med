import {Component, OnDestroy, OnInit} from "@angular/core";
import {Treatment} from "../models/treatment";
import {TreatmentService} from "./treatment.service";
import {HttpErrorResponse} from "@angular/common/http";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ADAService} from "../storage/data-access-service";
import {Service} from "../models/service";
import {ServiceService} from "../service/service.service";
import {User} from "../models/user";
import {MyErrorHandler} from "../handler/my-error-handler.service";
import {Specialty} from "../models/specialty";
import {DoctorService} from "../doctor/doctor.service";
import {Router} from "@angular/router";
import {DTService} from "../storage/data-transfer-service";
import {UserService} from "../user/user.service";
import {DoctorDto} from "../models/doctor.dto";

@Component({
  selector: 'treat-app',
  templateUrl: './treatment.component.html'
})
export class TreatmentComponent implements OnInit, OnDestroy{

  page: number = 1;

  treatments: Treatment[];
  services: Service[];
  doctors: DoctorDto[];
  patients: User[];
  specialties: Specialty[];

  doctorFreeTime: string[];
  hasFreeTime: boolean = true;
  editDate: string;
  editTime: string;

  editTreatment: Treatment;
  deleteTreatment: Treatment;

  errorMessage: string;
  errorOnForm: boolean=false;

  specialtyIdFromOutside: number;
  serviceFromOutside: Service;
  doctorFromOutside: DoctorDto;

  user: User;
  patientId:number;

  treatmentForm: FormGroup;
  isDateBeforeDoctor: boolean = false;

  constructor(private treatmentService: TreatmentService,
              private serviceService: ServiceService,
              private doctorService: DoctorService,
              private adaService: ADAService,
              private userService: UserService,
              private myErrorHandler: MyErrorHandler,
              private formBuilder: FormBuilder,
              private router:Router,
              private dtService: DTService) {
    this.treatmentForm = this.formBuilder.group({
      specialty: [''],
      service: [''],
      doctor: [''],
      patient: [''],
      date: [''],
      time: ['']
    });
  }

  ngOnInit(): void {

    if (localStorage.getItem('username') !== null) {
      this.userService.getUserByUsername(localStorage.getItem('username')).subscribe(
        (response: User) => {
          this.user = response;
          this.getSpecialties();

          if (this.user.userRole === 'ROLE_ADMIN') {
            this.getTreatments();
            this.getPatients();
          }

          if (this.user.userRole === 'ROLE_USER' || this.user.userRole === 'ROLE_DOCTOR') {
            this.doctorFromOutside = this.dtService.doctor;
            this.serviceFromOutside = this.dtService.service;
            this.specialtyIdFromOutside = this.dtService.specialtyId;
            if (this.user.userRole === 'ROLE_USER') {
              this.patientId = this.user.id;
            }
            else {
              this.getPatients();
            }
            this.doSomeOperation(null, 'add');
          }
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    }

    this.treatmentForm = new FormGroup({
      specialty: new FormControl('', Validators.required),
      service: new FormControl('', Validators.required),
      doctor: new FormControl('', Validators.required),
      patient: new FormControl('', Validators.required),
      date: new FormControl('', Validators.required),
      time: new FormControl('', Validators.required)
    });
  }

  ngOnDestroy(): void {
    this.dtService.specialtyId = undefined;
    this.dtService.service = undefined;
    this.dtService.doctor = undefined;
  }

  getSpecialties() {
    this.adaService.getSpecialties().subscribe(
      (response: Specialty[]) => {
        this.specialties = response;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  getServicesAndDoctorsBySpecialtyId(specialty: any) {

    this.treatmentForm.patchValue({
      service: '',
      doctor: '',
      date:'',
      time: ''
    });
    this.getServicesBySpecialtyId(specialty);
    this.getDoctorsBySpecialtyId(specialty);
  }

  getServicesBySpecialtyId(specialty: any) {
    this.serviceService.getServicesBySpecialtyId(parseInt(specialty)).subscribe(
      (response: Service[]) => {
        this.services = response;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  getDoctorsBySpecialtyId(specialty: any) {
    this.doctorService.getDoctorsBySpecialtyId(parseInt(specialty)).subscribe(
      (response: DoctorDto[]) => {
        this.doctors = response;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  getDoctorFreeTime(doctor: any, date: string) {

    if (doctor === '') {
      this.treatmentForm.patchValue({
        date: ''
      });
      this.isDateBeforeDoctor = true;
    }
    else {
      this.treatmentService.getDoctorFreeTime(parseInt(doctor), date).subscribe(
        (response: string[]) => {
          this.doctorFreeTime = response;
          if (response.length === 0) {
            this.hasFreeTime = false;
          } else {
            this.hasFreeTime = true;
            if (this.editDate !== undefined) {
              if (this.editDate === date) {
                this.doctorFreeTime.push(this.editTime);
              }
            }
            this.doctorFreeTime.sort();
          }
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    }
  }

  onAddTreatment() {

    if (this.treatmentForm.valid) {
      document.getElementById('add-treatment-form').click();

      let dateTime = this.treatmentForm.value.date + 'T' + this.treatmentForm.value.time;

      if (this.user.userRole !== 'ROLE_USER') {
        this.patientId = parseInt(this.treatmentForm.value.patient);
      }

      this.treatmentService.updateDoctorSchedule(
        parseInt(this.treatmentForm.value.doctor),
        this.patientId,
        dateTime,
        null).subscribe(
        () => {
          this.treatmentService.createTreatment(
            parseInt(this.treatmentForm.value.service),
            parseInt(this.treatmentForm.value.doctor),
            this.patientId,
            dateTime).subscribe(
            () => {
              if (this.user.userRole === 'ROLE_ADMIN') {
                this.getTreatments();
                this.page = 1;
              }
              this.redirectToLK();
            },
            (error: HttpErrorResponse) => {
              this.handleErrorResponse(error);
            }
          );
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.errorOnForm = true;
    }
  }

  getTreatments(): void {
    this.treatmentService.getTreatments().subscribe(
      (response: Treatment[]) => {
        this.treatments= response;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  doSomeOperation(treatment: Treatment, operation: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if (treatment !== null) {
      this.treatmentService.getTreatmentById(treatment.id).subscribe(
        () => {
          if (operation === 'edit') {
            this.errorOnForm = false;
            this.editTreatmentForm(treatment);
            button.setAttribute('data-target', '#updateTreatmentModal');
          }

          if (operation === 'delete') {
            this.deleteTreatment = treatment;
            button.setAttribute('data-target', '#deleteTreatmentModal');
          }
          container.appendChild(button);
          button.click();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
          this.getTreatments();
        }
      );
    } else {
      if (operation === 'add') {
        this.prepareToAddTreatment();
        this.errorOnForm = false;
        button.setAttribute('data-target', '#addTreatmentModal');
      }

      if (operation === 'showError') {
        button.setAttribute('data-target', '#errorModal');
      }
      container.appendChild(button);
      button.click();
    }
  }

  getPatients() {
    this.adaService.getUsers().subscribe(
      (response: User[]) => {
        this.patients = response;
      },
      (error: HttpErrorResponse) => {
      this.handleErrorResponse(error);
      }
    );
  }

  onUpdateTreatment(treatmentId: number) {

    if (this.treatmentForm.valid) {
      document.getElementById('edit-treatment-form').click();
      this.errorOnForm = false;
      let dateTime = this.treatmentForm.value.date + 'T' + this.treatmentForm.value.time;

      this.treatmentService.updateDoctorSchedule(
        this.editTreatment.doctor.user.id,
        parseInt(this.treatmentForm.value.patient),
        dateTime,
        this.editDate + 'T' + this.editTime).subscribe(
        () => {
          this.treatmentService.updateTreatment(treatmentId, this.treatmentForm.value.service,
            this.treatmentForm.value.doctor,this.treatmentForm.value.patient, this.treatmentForm.value.date + 'T' + this.treatmentForm.value.time).subscribe(
            (res:any) => {
              this.getTreatments();
            },
            (error: HttpErrorResponse) => {
              this.handleErrorResponse(error);
            }
          );
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.errorOnForm = true;
    }
  }

  onDeleteTreatment(treatmentId: number) {
    this.treatmentService.deleteTreatment(treatmentId).subscribe(
      () => {
        this.getTreatments();
        this.page = 1;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  editTreatmentForm(treatment: Treatment) {
    this.getServicesBySpecialtyId(treatment.service.specialty.id);
    this.editTreatment = treatment;
    this.retrieveDateAndTime(treatment);

    this.getDoctorFreeTime(treatment.doctor.user.id, this.editDate);
    this.patchEditFormValues();

  }

  patchEditFormValues() {
    this.treatmentForm.patchValue({
      specialty: this.editTreatment.service.specialty.id,
      service: this.editTreatment.service.id,
      doctor: this.editTreatment.doctor.user.id,
      patient: this.editTreatment.patient.id,
      date: this.editDate,
      time: this.editTime
    });
  }

  retrieveDateAndTime(treatment: Treatment) {
    this.editDate = treatment.appointment.toString().substr(0, 10);
    this.editTime = treatment.appointment.toString().substr(11, 8);
  }

  prepareToAddTreatment() {
    this.treatmentForm.reset();

    if ((this.specialtyIdFromOutside !== undefined)
      || (this.doctorFromOutside !== undefined)
      || (this.serviceFromOutside !== undefined)) {
      this.prepareWithSomeParams();
    }
    if (this.user.userRole === 'ROLE_USER') {
      this.treatmentForm.patchValue({
        patient: this.patientId
      });
    }
  }

  prepareWithSomeParams() {

    if (this.specialtyIdFromOutside !== undefined) {
      this.treatmentForm.patchValue({
        specialty: this.specialtyIdFromOutside
      });
      this.getServicesAndDoctorsBySpecialtyId(this.specialtyIdFromOutside);
    }

    if (this.serviceFromOutside !== undefined) {
      this.getServicesAndDoctorsBySpecialtyId(this.serviceFromOutside.specialty.id);
      this.treatmentForm.patchValue({
        specialty: this.serviceFromOutside.specialty.id,
        service: this.serviceFromOutside.id
      });
    }

    if (this.doctorFromOutside !== undefined) {
      this.getServicesAndDoctorsBySpecialtyId(this.doctorFromOutside.specialty.id);
      this.treatmentForm.patchValue({
        specialty: this.doctorFromOutside.specialty.id,
        doctor: this.doctorFromOutside.id
      });
    }
  }

  handleErrorResponse(error: HttpErrorResponse) {
    this.errorMessage = this.myErrorHandler.handleError(error);
    this.doSomeOperation(null, 'showError');
    this.page = 1;
  }

  redirectToLK() {
    if (this.user.userRole !== 'ROLE_ADMIN') {
      this.specialtyIdFromOutside = undefined;
      this.doctorFromOutside = undefined;
      this.serviceFromOutside = undefined;
      this.router.navigate(['/users/lk']);
    }
  }

}
