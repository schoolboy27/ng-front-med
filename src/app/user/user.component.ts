import {Component, OnDestroy, OnInit} from "@angular/core";
import {User} from "../models/user";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "./user.service";
import {HttpErrorResponse} from "@angular/common/http";
import {AuthService} from "../auth/auth.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MyErrorHandler} from "../handler/my-error-handler.service";
import {Subscription} from "rxjs";
import {Center} from "../models/center";
import {CenterService} from "../center/center.service";


@Component({
  selector: 'user-component',
  templateUrl: './users.component.html',
  styleUrls: ['./user-component-style.scss']
})
export class UserComponent implements OnInit, OnDestroy{

  page: number =1;
  userForm: FormGroup;
  editUserForm: FormGroup;

  users: User[];
  editUser: User;
  deleteUser: User;

  roles: string[] = ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_DOCTOR'];
  centers: Center[];

  userRole: string;
  private sub: Subscription;

  errorMessage: string;
  errorOnForm:  boolean = false;

  emailAlreadyUsed: boolean=false;
  passwordConfirm: boolean=true;
  agePass: boolean=true;


  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private userService: UserService,
              private loginService: AuthService,
              private centerService: CenterService,
              private activatedRoute: ActivatedRoute,
              private myErrorHandler: MyErrorHandler
  ) {

    this.userForm = this.formBuilder.group({
      lastName: [''],
      firstName: [''],
      middleName: [''],
      login: [''],
      password: [''],
      birthDate: [''],
      repPassword: [''],
      userRole: [''],
      center: ['']
    });
    this.editUserForm = this.formBuilder.group({
      lastName: [''],
      firstName: [''],
      middleName: [''],
      birthDate: [''],
      userRole: [''],
      center: ['']
    });
  }

  ngOnInit(): void {

    if (localStorage.getItem('username') !== null) {
      this.userService.getUserByUsername(localStorage.getItem('username')).subscribe(
        (response: User) => {
          this.userRole = response.userRole;
          if (this.userRole === 'ROLE_ADMIN') {
            this.getUsers();
            this.getCenters();
          }
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.router.navigate(['/users/login']);
    }

    this.userForm = new FormGroup({
      lastName: new FormControl('', [Validators.required, Validators.pattern('^[A-Z][a-z]*$')]),
      firstName: new FormControl('', [Validators.required, Validators.pattern('^[A-Z][a-z]*$')]),
      middleName: new FormControl('', [Validators.required, Validators.pattern('^[A-Z][a-z]*$')]),
      login: new FormControl('', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9]+[a-z0-9.-]+@[a-z0-9.-]+\.[a-z]{2,6}$')]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      birthDate: new FormControl('', Validators.required),
      repPassword: new FormControl('', Validators.required),
      userRole: new FormControl('ROLE_USER', Validators.required),
      center: new FormControl('', Validators.required)
    });
    this.editUserForm = new FormGroup({
      lastName: new FormControl('', [Validators.required, Validators.pattern('^[A-Z][a-z]*$')]),
      firstName: new FormControl('', [Validators.required, Validators.pattern('^[A-Z][a-z]*$')]),
      middleName: new FormControl('', [Validators.required, Validators.pattern('^[A-Z][a-z]*$')]),
      birthDate: new FormControl('', Validators.required),
      userRole: new FormControl('ROLE_USER', Validators.required),
      center: new FormControl('', Validators.required)
    });
  }

  ngOnDestroy(): void {
    //this.sub.unsubscribe();
  }

  public getUsers(): void {
    this.userService.getUsers().subscribe(
      (response: User[]) => {
        this.users = response;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  getCenters() {
    this.centerService.getCenters().subscribe(
      (response: Center[]) => {
        this.centers = response;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  doSomeOperation(user: User, operation: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if (user !== null) {
      this.userService.getUserById(user.id).subscribe(
        (response: User) => {
          if (operation === 'edit') {
            this.patchValues(user);
            button.setAttribute('data-target', '#updateUserModal');
          }
          if (operation === 'delete') {
            this.deleteUser = user;
            button.setAttribute('data-target', '#deleteUserModal');
          }
          container.appendChild(button);
          button.click();
        },
        (error:HttpErrorResponse) => {
          this.handleErrorResponse(error);
          this.getUsers();
        }
      );
    } else {
      if (operation === 'add') {
        this.errorOnForm = false;
        button.setAttribute('data-target', '#addUserModal');
      }
      if (operation === 'showError') {
        button.setAttribute('data-target', '#errorModal');
      }
      container.appendChild(button);
      button.click();
    }

  }

  createUser() {

    if (this.userForm.valid) {
      this.agePass = this.userService.checkAge(this.userForm.value.birthDate);
      this.passwordConfirm = this.userService.checkPasswordEquality(
        this.userForm.value.password,
        this.userForm.value.repPassword
      );

      if (this.passwordConfirm && this.agePass) {

        let center = this.centers.filter(center => center.id === parseInt(this.userForm.value.center))[0];

        let user: User = this.userForm.value;
        user.center = center;

        this.userService.createUser(user).subscribe(
          (res: any) => {
            this.emailAlreadyUsed = false;
            document.getElementById('add-user-form').click();
            this.getUsers();
            this.page = 1;
          },
          (error: HttpErrorResponse) => {
            if (error.status === 403) {
              this.emailAlreadyUsed = true;
            } else {
              this.handleErrorResponse(error);
            }
          });
      }
    } else {
      this.errorOnForm = true;
    }
  }

  onUpdateUser() {

    if (this.editUserForm.valid) {
      this.agePass = this.userService.checkAge(this.editUserForm.value.birthDate);

      if (this.agePass) {
        document.getElementById('edit-user-form').click();
        let center = this.centers.filter(center => center.id === parseInt(this.editUserForm.value.center))[0];
        let user: User = this.editUserForm.value;
        user.center = center;

        this.userService.updateUser(this.editUser.id, user).subscribe(
          (res: any) => {
            this.getUsers();
          },
          (error: HttpErrorResponse) => {
            this.handleErrorResponse(error);
          }
        );
      }
    } else {
      this.errorOnForm = true;
    }
  }

  onDeleteUser(userId: number) {
    this.userService.deleteUser(userId).subscribe(
      (res:any) => {
        this.getUsers();
        this.page = 1;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  patchValues(user: User) {
    this.errorOnForm = false;
    this.editUser = user;
    this.editUserForm.reset();
    this.editUserForm.patchValue({
      lastName: user.lastName,
      firstName: user.firstName,
      middleName: user.middleName,
      birthDate: user.birthDate,
      userRole: user.userRole
    });

    if (user.center !== null) {
      this.editUserForm.patchValue({
        center: user.center.id
      });
    } else {
      this.editUserForm.patchValue({
        center: this.centers[0].id
      });
    }
  }

  handleErrorResponse(error: HttpErrorResponse) {
    this.errorMessage = this.myErrorHandler.handleError(error);
    this.doSomeOperation(null, 'showError');
    this.page = 1;
  }
}
