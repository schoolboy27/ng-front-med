import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../models/user";
import {environment} from "src/environments/environment";
import {AuthService} from "../auth/auth.service";
import {UserDto} from "../models/user.dto";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiServerUrl = environment.apiBaseUrl;

  private currentYear = parseInt((new Date()).toISOString().substr(0,4));
  private currentMonth = parseInt((new Date()).toISOString().substr(6,2));

  constructor(private http: HttpClient, private loginService: AuthService) { }


  public getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${this.apiServerUrl}/users/all`);//, {headers : this.header});
  }

  public getUserById(userId: number) {
    return this.http.get(`${this.apiServerUrl}/users/${userId}`);
  }

  public getUserByUsername(username: string) {
    return this.http.post(`${this.apiServerUrl}/users/find/username`, username);
  }

  public getInterlocutorsForAdmin() {
    return this.http.get(`${this.apiServerUrl}/users/all/interlocutors`);
  }

  getUsersNotDoctors() {
    return this.http.get(`${this.apiServerUrl}/users/all/not/doctors`);
  }

  public createUser(user: any) {
    return this.http.post<any>(`${this.apiServerUrl}/users/create`, user);//, catchError(this.handleError);
  }

  public updateUser(userId: number, user: User): Observable<User> {
    return this.http.post<any>(`${this.apiServerUrl}/users/update/${userId}`, user);
  }

  public deleteUser(userId: number): Observable<User> {
    return this.http.delete<User>(`${this.apiServerUrl}/users/delete/${userId}`);
  }

  getPersonsStrings(persons: UserDto[]): string[] {
    let result: string[] = [];
    persons.forEach(person => {
      result.push(
        person.id + '. '
        + person.lastName + ' '
        + person.firstName + ' '
        + person.middleName + ', '
        + person.role);
    });
    return Array.from(new Set(result));
  }

  checkPasswordEquality(pass1: string, pass2: string): boolean {
    return pass1 === pass2;
  }

  checkAge(birthDate: string) {
    let birthYear = parseInt(birthDate.substr(0,4));
    let birthMonth = parseInt(birthDate.substr(6,2));
    let age = this.currentYear - birthYear + (this.currentMonth - birthMonth)/12;
    return age >= 18;
  }
}
