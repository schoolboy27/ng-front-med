import {AfterViewChecked, Component, ElementRef, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {User} from "../models/user";
import {UserService} from "../user/user.service";
import {TreatmentService} from "../treatment/treatment.service";
import {Doctor} from "../models/doctor";
import {Treatment} from "../models/treatment";
import {MmessageService} from "../message/mmessage.service";
import {Message} from "../models/message";
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {DoctorDto} from "../models/doctor.dto";
import {UserDto} from "../models/user.dto";
import {Observable, Subscription, timer} from "rxjs";
import {Router} from "@angular/router";
import {MyErrorHandler} from "../handler/my-error-handler.service";
import {HttpErrorResponse} from "@angular/common/http";


@Component({
  selector: 'chat-app',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.style.scss']
})
export class ChatComponent implements OnInit, OnDestroy, AfterViewChecked{
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;

  user: User;
  interlocutorId: number;
  interlocutors: UserDto[];
  interlocutorsAsStrings: string[] = [];

  messages: Message[];
  messageForm: FormGroup;

  source: Observable<number> = timer(1000, 3000);
  subscription: Subscription;

  errorMessage: string;

  constructor(private userService: UserService,
              private treatmentService: TreatmentService,
              private messageService: MmessageService,
              private myErrorHandler: MyErrorHandler,
              private router: Router,
              private formBuilder: FormBuilder) {
    this.messageForm = this.formBuilder.group({
      topic: '',
      message: ''
    });
  }

  ngOnInit(): void {

    if (localStorage.getItem('username')) {
      this.userService.getUserByUsername(localStorage.getItem('username')).subscribe(
        (res: User) => {
          this.user = res;
          this.getInterlocutors();

        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.router.navigate(['/users/login']);
    }


    this.messageForm = new FormGroup({
      topic: new FormControl('info', [Validators.required]),
      message: new FormControl('', [Validators.required, Validators.minLength(2)])
    });

  }

  ngOnDestroy(): void {
    if (this.subscription !== undefined) {
      this.subscription.unsubscribe();
    }
  }

  ngAfterViewChecked(): void {
    this.scrollToBottom();
  }

  getInterlocutors() {
    if (this.user.userRole === 'ROLE_USER') {
      this.getUserInterlocutors(this.user.id);
    }
    if (this.user.userRole === 'ROLE_DOCTOR') {
      this.getDoctorInterlocutors(this.user.id);
    }
    if (this.user.userRole === 'ROLE_ADMIN') {
      this.getAllInterlocutors();
    }
  }

  getUserInterlocutors(userId: number) {
    this.treatmentService.getDoctorsByPatientId(userId).subscribe(
      (res: UserDto[]) => {
       this.getInterlocutorsStrings(res);
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  getDoctorInterlocutors(userId: number) {
    this.treatmentService.getDoctorInterlocutors(userId).subscribe(
      (response: UserDto[]) => {
        this.interlocutors = response;
        this.getInterlocutorsStrings(response);
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  getAllInterlocutors() {
    this.userService.getInterlocutorsForAdmin().subscribe(
      (response: UserDto[]) => {
        this.interlocutors = response;
        this.getInterlocutorsStrings(response);
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  getMessagesByRecipient(notUserId: string) {
    if (this.subscription !== undefined) {
      this.subscription.unsubscribe();
    }
    let id = parseInt(notUserId);
    this.interlocutorId = id;
    this.getMessages(id);
    this.subscription = this.source.subscribe(value => this.getMessages(id));

  }

  getMessages(interlocutor: number) {
    this.messageService.getMessagesByTwoUsers(interlocutor, this.user.id).subscribe(
      (res) => {
        this.messages = res;

      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }


  sendMessage(messageForm: FormGroup) {

    if (messageForm.valid) {
      this.messageService.createMessage(this.user.id, this.interlocutorId, messageForm).subscribe(
        (res) => {
          this.getMessages(this.interlocutorId);
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
      this.messageForm.reset("");
      this.messageForm.patchValue({
        topic: 'info'
      });
    }
  }

  scrollToBottom() {
    if (this.myScrollContainer !== undefined) {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    }
  }

  getInterlocutorsStrings(users: UserDto[]) {
    this.interlocutorsAsStrings = this.userService.getPersonsStrings(users);
  }

  handleErrorResponse(error: HttpErrorResponse) {
    this.errorMessage = this.myErrorHandler.handleError(error);
    this.showError();
  }

  showError() {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    button.setAttribute('data-target', '#errorModal');
    container.appendChild(button);
    button.click();
  }
}
