import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {DoctorDto} from "../models/doctor.dto";
import {Service} from "../models/service";
import {Doctor} from "../models/doctor";

@Injectable()
export class DTService {



  private isLog = new Subject<boolean>();
  public isLog$ = new Subject<boolean>();

  public user$ = new Subject<string>();

  public userRole$ = new Subject<string>();
  public userRoleV: string;

  public specialtyId$: Subject<any> = new Subject<any>();
  public specialtyId: number;

  public service$: Subject<Service> = new Subject<Service>();
  public service: Service;

  public doctor$: Subject<DoctorDto> = new Subject<DoctorDto>();
  public doctor: DoctorDto;
  public reviewedDoctor: Doctor;


  /*--------check something--------*/
  public role: string;
  public getRole() : string {
    return this.role;
  }

  public setRole(role: string) {
    this.role = role;
  }


  /*-------------------------------*/

  public changeLog(isLog: boolean) {
    this.isLog$.next(isLog);

  }

  public changeLoggedUser(userLogged: string) {
    this.user$.next(userLogged);
  }

  public changeUserRole(userRole: string) {
    this.userRole$.next(userRole);
    this.userRoleV = userRole;
  }

  public changeSpecialtyId(specialtyId: number) {
    this.specialtyId$.next(specialtyId);
  }

  public changeService(service: Service) {
    this.service$.next(service);
  }

  public changeDoctor(doctor: DoctorDto) {
    this.doctor$.next(doctor);
  }

}
