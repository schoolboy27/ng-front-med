import {Injectable} from "@angular/core";
import {SpecialtyService} from "../specialty/specialty.service";
import {ServiceService} from "../service/service.service";
import {DoctorService} from "../doctor/doctor.service";
import {UserService} from "../user/user.service";
import {Service} from "../models/service";
import {Specialty} from "../models/specialty";
import {Doctor} from "../models/doctor";
import {User} from "../models/user";
import {Observable} from "rxjs";
import {CenterService} from "../center/center.service";
import {Center} from "../models/center";
import {LocationService} from "../location/location.service";
import {MyLocation} from "../models/mylocation";


@Injectable()
export class ADAService {

  constructor(private specialtyService: SpecialtyService,
              private serviceService: ServiceService,
              private doctorService: DoctorService,
              private userService: UserService,
              private centerService: CenterService,
              private locationService: LocationService,
  ) {}

  getServices(): Observable<Service[]> {
    return this.serviceService.getServices();
  }

  getDoctors(): Observable<Doctor[]> {
    return this.doctorService.getDoctors();
  }

  getUsers(): Observable<User[]> {
    return this.userService.getUsers();
  }

  getSpecialties(): Observable<Specialty[]> | any {
    return this.specialtyService.getSpecialties();
  }

  getCenters(): Observable<Center[]> {
    return this.centerService.getCenters();
  }

  getLocations(): Observable<MyLocation[]> {
    return this.locationService.getLocations();
  }

  getUserByUsername(username: string) {
    return this.userService.getUserByUsername(username);
  }

}
