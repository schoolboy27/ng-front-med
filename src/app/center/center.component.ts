import {Component, OnInit} from "@angular/core";
import {CenterService} from "./center.service";
import {User} from "../models/user";
import {HttpErrorResponse} from "@angular/common/http";
import {Center} from "../models/center";
import {ADAService} from "../storage/data-access-service";
import {MyLocation} from "../models/mylocation";
import {NgForm} from "@angular/forms";
import {MyErrorHandler} from "../handler/my-error-handler.service";
import {Router} from "@angular/router";
import {LocationService} from "../location/location.service";


@Component({
  selector: 'center-app',
  templateUrl: './center.component.html'
})
export class CenterComponent implements OnInit{

  page: number = 1;
  public centers: Center[] | undefined;
  locations: MyLocation[];
  locationsForEdit: MyLocation[] = [];

  role: string;
  errorMessage: string;
  errorOnForm: boolean = false;

  editCenter: Center;
  deleteCenter: Center;

  constructor(private centerService: CenterService,
              private adaService: ADAService,
              private locationService: LocationService,
              private myErrorHandler: MyErrorHandler,
              private router: Router) {
  }

  ngOnInit(): void {
    if (localStorage.getItem('username') !== null) {
      this.adaService.getUserByUsername(localStorage.getItem('username')).subscribe(
        (response: User) => {
          this.role = response.userRole;
          if (this.role === 'ROLE_ADMIN') {
            this.getCenters();
            this.getLocations();
          }
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.router.navigate(['/users/login']);
    }
  }

  public getCenters(): void {
    this.centerService.getCenters().subscribe(
      (response: Center[]) => {
        this.centers = response;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  getLocations() {
    this.locationService.getFreeLocations().subscribe(
      (res) => {
        this.locations = res;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  doSomeOperation(center: Center, operation: string) {

    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if (center !== null) {
      this.centerService.getCenterById(center.id).subscribe(
        (response: Center) => {
          if (operation === 'edit') {
            this.errorOnForm = false;
            this.editCenter = center;
            this.editLocationsList();
            button.setAttribute('data-target', '#updateCenterModal');
          }

          if (operation === 'delete') {
            this.deleteCenter = center;
            button.setAttribute('data-target', '#deleteCenterModal');
          }
          container.appendChild(button);
          button.click();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
          this.getCenters();
        }
      );
    } else {
      if (operation === 'add') {
        this.errorOnForm = false;
        button.setAttribute('data-target', '#addCenterModal');
      }
      if (operation === 'showError') {
        button.setAttribute('data-target', '#errorModal');
      }
      container.appendChild(button);
      button.click();
    }
  }


  onAddCenter(addForm: NgForm) {

    if (addForm.valid) {

      this.errorOnForm = false;
      this.centerService.createCenter(parseInt(addForm.value.location)).subscribe(
        (res: any) => {
          document.getElementById('add-center-form').click();
          this.getCenters();
          this.page = 1;
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.errorOnForm = true;
    }
  }

  onUpdateCenter(centerId: number, editForm: NgForm) {

    if (editForm.valid) {

      this.errorOnForm = false;
      this.centerService.updateCenter(centerId, editForm).subscribe(
        (res: any) => {
          this.getCenters();
          document.getElementById('edit-center-form').click();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.errorOnForm = true;
    }
  }

  editLocationsList() {
    this.locationsForEdit = [];
    this.locations.forEach(location => {
      this.locationsForEdit.push(location);
    });
    this.locationsForEdit.push(this.editCenter.location);
  }

  onDeleteCenter(centerId: number) {
    this.centerService.deleteCenter(centerId).subscribe(
      (res: any) => {
        this.getCenters();
        this.page = 1;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  handleErrorResponse(error: HttpErrorResponse) {
    this.errorMessage = this.myErrorHandler.handleError(error);
    this.doSomeOperation(null, 'showError');
    this.getCenters();
    this.page = 1;
  }


}
