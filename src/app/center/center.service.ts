import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Center} from "../models/center";


@Injectable()
export class CenterService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {
  }

  public getCenters(): Observable<Center[]> {
    return this.http.get<Center[]>(`${this.apiServerUrl}/centers/all`);
  }

  getCenterById(centerId: number) {
    return this.http.get(`${this.apiServerUrl}/centers/${centerId}`);
  }

  createCenter(locationId: number) {
    return this.http.post(`${this.apiServerUrl}/centers/create/loc/${locationId}`, {});
  }

  updateCenter(centerId: number, editForm: any) {
    return this.http.post(`${this.apiServerUrl}/centers/update/${centerId}`,
      {
        'locationId': parseInt(editForm.value.location)
      });
  }

  deleteCenter(centerId: number) {
    return this.http.delete(`${this.apiServerUrl}/centers/delete/${centerId}`);
  }


}
