import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "src/environments/environment";
import {Router} from "@angular/router";
import {FormGroup} from "@angular/forms";


@Injectable()
export class AuthService {

  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient,
              public router: Router) {
  }

  doLogin(form: FormGroup) {

    return this.http.post<any>(`${this.apiServerUrl}/users/login`, {
      'username': form.value.username,
      'password': form.value.password
    });
  }

  doLogout() {
    return this.http.post(`${this.apiServerUrl}/users/logout`, {});
  }

}
