import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../models/user";
import {Router} from "@angular/router";
import {UserService} from "../user/user.service";
import {CenterService} from "../center/center.service";
import {Center} from "../models/center";
import {HttpErrorResponse} from "@angular/common/http";
import {MyErrorHandler} from "../handler/my-error-handler.service";

@Component({
  selector: 'reg-app',
  templateUrl: './reg.component.html',
  styleUrls: ['./reg-style.scss']
})
export class RegComponent implements OnInit{

  singUpForm: FormGroup;
  centers: Center[];
  emailAlreadyUsed: boolean = false;
  passwordConfirm: boolean = true;
  agePass: boolean = true;

  errorMessage: string;
  errorOnForm: boolean = false;

  constructor(public formBuilder: FormBuilder,
              private router: Router,
              private userService: UserService,
              private centerService: CenterService,
              private myErrorHandler: MyErrorHandler) {
    this.singUpForm = this.formBuilder.group({
      lastName: [''],
      firstName: [''],
      middleName: [''],
      login: [''],
      password: [''],
      birthDate: [''],
      repPassword: [''],
      center: [''],
      userRole: ['']
    });
  }

  ngOnInit(): void {
    this.singUpForm = new FormGroup({
      lastName: new FormControl('', [Validators.required, Validators.pattern('^[A-Z][a-z]*$')]),
      firstName: new FormControl('', [Validators.required, Validators.pattern('^[A-Z][a-z]*$')]),
      middleName: new FormControl('', [Validators.required, Validators.pattern('^[A-Z][a-z]*$')]),
      login: new FormControl('', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9]+[a-z0-9.-]+@[a-z0-9.-]+\.[a-z]{2,6}$')]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      birthDate: new FormControl('', [Validators.required, Validators.maxLength(10)]),
      repPassword: new FormControl('', Validators.required),
      center: new FormControl('', Validators.required),
      userRole: new FormControl('ROLE_USER')
    });
    this.getCenters();
  }

  getCenters() {
    this.centerService.getCenters().subscribe(
      (response: Center[]) => {
        this.centers = response;
      },
      (error: HttpErrorResponse) => {
        this.myErrorHandler.handleError(error);
      }
    );
  }

  createUser() {

    if (this.singUpForm.valid) {
      this.agePass = this.userService.checkAge(this.singUpForm.value.birthDate);
      this.passwordConfirm = this.userService.checkPasswordEquality(
        this.singUpForm.value.password,
        this.singUpForm.value.repPassword
      );

      if (this.passwordConfirm && this.agePass) {

        let center = this.centers.filter(center => center.id === parseInt(this.singUpForm.value.center))[0];
        let user: User = this.singUpForm.value;
        user.center = center;

        this.userService.createUser(user).subscribe(
          (res: any) => {
            this.emailAlreadyUsed = false;
            this.router.navigate(['/users/login']);
          },
          (error) => {
            this.emailAlreadyUsed = true;
          });
      }
    } else {
      this.errorOnForm = true;
    }
  }

}
