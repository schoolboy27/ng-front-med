import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let authRequest = request;
    const authToken = localStorage.getItem('token');
    if (authToken !== null) {
      authRequest = request.clone({
        headers: request.headers.set('X-Requested-With', 'XMLHttpRequest'),
        setHeaders: {
          Authorization: authToken
        }
      });
    }

    return next.handle(authRequest);
  }
}
