import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {AuthService} from "./auth.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {DTService} from "../storage/data-transfer-service";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'login-app',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit{

  singInForm: FormGroup;
  catchSomeError: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private loginService: AuthService,
              private dtService: DTService
              ) {
    this.singInForm = this.formBuilder.group({
      username: [''],
      password: ['']
    });
  }

  ngOnInit(): void {
    this.singInForm= new FormGroup({
      username: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(4)])
    });
  }

  doLogin() {
    this.loginService.doLogin(this.singInForm).subscribe(
      (response: any) => {
        this.catchSomeError = false;

        localStorage.setItem('token', response.token);
        localStorage.setItem('username', response.username);

        this.router.navigate(['/']).then(() => {
          window.location.reload();
        });
      },
      (error: HttpErrorResponse) => {
        this.catchSomeError = true;
      }
    );
  }

  doSingUp() {
    this.router.navigate(['/users/create']);
  }
}
