import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from "./models/user";
import {UserService} from "./user/user.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";
import {AuthService} from "./auth/auth.service";
import {DTService} from "./storage/data-transfer-service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, OnDestroy {

  isLoggedInSystem: boolean=false;
  username: string = '';
  role: string;

  constructor(private router: Router,
              private dtService: DTService,
              private userService: UserService,
              private authService: AuthService
              ) {}

  ngOnInit(): void {

    if (localStorage.getItem('username') !== null) {
      this.isLoggedInSystem = true;
      this.getUserRole();
    }
  }

  ngOnDestroy(): void {
    localStorage.removeItem('token');
  }

  getUserRole() {
    this.userService.getUserByUsername(localStorage.getItem('username')).subscribe(
      (res: User) => {
        this.role = res.userRole;
        this.username = res.login;
      },
      (error: HttpErrorResponse) => {
        if (error.status === 401) {
          localStorage.clear();
          this.router.navigate(['/users/login']).then(() => {
            window.location.reload()});
        }
      }
    );
  }

  getUserComponent() {
    this.router.navigate(['/users/all']);
  }

  getSpecialtyComponent() {
    this.router.navigate(['/specialties/all']);
  }

  getServiceComponent() {
    this.router.navigate(['/services/all']);
  }

  getLocationComponent() {
    this.router.navigate(['/locations/all']);
  }

  getCenterComponent() {
    this.router.navigate(['/centers/all']);
  }

  getMessageComponent() {
    this.router.navigate(['/messages/all']);
  }

  getDoctorComponent() {
    this.router.navigate(['/doctors/all']);
  }

  getReviewComponent() {
    this.router.navigate(['/reviews/all']);
  }

  getTreatmentComponent() {
    this.router.navigate(['/treatments/all']);
  }

  getAuthorizationComponent() {
    this.router.navigate(['/users/create']);
  }

  getLoginComponent() {
    this.router.navigate(['/users/login']);
  }

  doLogout() {

    this.isLoggedInSystem = false;
    this.role = '';
    this.username = '';

    this.authService.doLogout().subscribe(
      () => {
        localStorage.clear();
      }
    );
    this.router.navigate(['/users/login']);
  }

  showLK() {
    this.router.navigate(['/users/lk']);
  }
}
