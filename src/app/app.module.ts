import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {UserService} from "./user/user.service";
import {HTTP_INTERCEPTORS, HttpClientModule, HttpClientXsrfModule} from "@angular/common/http";
import {SpecialtyService} from "./specialty/specialty.service";
import {RouterModule, Routes} from "@angular/router";
import {UserComponent} from "./user/user.component";
import {SpecialtiesComponent} from "./specialty/specialties.component";
import {ServiceService} from "./service/service.service";
import {ServiceComponent} from "./service/service.component";
import {LocationComponent} from "./location/location.component";
import {LocationService} from "./location/location.service";
import {CenterComponent} from "./center/center.component";
import {CenterService} from "./center/center.service";
import {MmessageComponent} from "./message/mmessage.component";
import {MmessageService} from "./message/mmessage.service";
import {DoctorComponent} from "./doctor/doctor.component";
import {DoctorService} from "./doctor/doctor.service";
import {ReviewComponent} from "./review/review.component";
import {TreatmentComponent} from "./treatment/treatment.component";
import {ReviewService} from "./review/review.service";
import {TreatmentService} from "./treatment/treatment.service";
import {RegComponent} from "./auth/reg.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {LoginComponent} from "./auth/login.component";
import {AuthService} from "./auth/auth.service";
import {AuthInterceptor} from "./auth/auth.interceptor";
import {ADAService} from "./storage/data-access-service";
import {DTService} from "./storage/data-transfer-service";
import {UnauthorizedErrorComponent} from "./errors/error-403/unauthorized-error.component";
import {MyErrorHandler} from "./handler/my-error-handler.service";
import {LKComponent} from "./lk/lk.component";
import {ChatComponent} from "./chat/chat.component";
import {NgxPaginationModule} from "ngx-pagination";
import {MainPageComponent} from "./main-page/main.page.component";


const appRoutes: Routes = [
  { path: 'users/all', component: UserComponent},
  { path: 'specialties/all', component: SpecialtiesComponent},
  { path: 'services/all', component: ServiceComponent},
  { path: 'locations/all', component: LocationComponent},
  { path: 'centers/all', component: CenterComponent},
  { path: 'messages/all', component: MmessageComponent},
  { path: 'doctors/all', component: DoctorComponent},
  { path: 'reviews/all', component: ReviewComponent},
  { path: 'treatments/all', component: TreatmentComponent},
  { path: 'users/login', component: LoginComponent },
  { path: 'users/create', component: RegComponent},
  { path: 'users/lk', component: LKComponent},
  { path: 'users/chat', component: ChatComponent},
  { path: '403', component: UnauthorizedErrorComponent},
  { path: '', component: MainPageComponent},
  { path: '**', redirectTo: ''}
];


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    SpecialtiesComponent,
    ServiceComponent,
    LocationComponent,
    CenterComponent,
    MmessageComponent,
    DoctorComponent,
    ReviewComponent,
    TreatmentComponent,
    LoginComponent,
    RegComponent,
    UnauthorizedErrorComponent,
    LKComponent,
    ChatComponent,
    MainPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    HttpClientXsrfModule.withOptions({cookieName: 'XSRF-TOKEN'}),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  providers: [
    UserService,
    SpecialtyService,
    ServiceService,
    LocationService,
    CenterService,
    MmessageService,
    DoctorService,
    ReviewService,
    TreatmentService,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    ADAService,
    DTService,
    MyErrorHandler
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
