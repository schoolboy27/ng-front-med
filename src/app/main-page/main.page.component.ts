import {Component, OnInit} from "@angular/core";
import {Center} from "../models/center";
import {Specialty} from "../models/specialty";
import {Service} from "../models/service";
import {Router} from "@angular/router";
import {CenterService} from "../center/center.service";
import {SpecialtyService} from "../specialty/specialty.service";
import {ServiceService} from "../service/service.service";
import {HttpErrorResponse} from "@angular/common/http";
import {MyErrorHandler} from "../handler/my-error-handler.service";


@Component({
  selector: 'main-page',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.style.scss']
})
export class MainPageComponent implements OnInit{

  errorMessage: string;

  centers: Center[];
  specialties: Specialty[];
  services: Service[];

  constructor(private router: Router,
              private centerService: CenterService,
              private specialtyService: SpecialtyService,
              private serviceService: ServiceService,
              private myErrorHandler: MyErrorHandler) {
  }

  ngOnInit(): void {
    this.getCenters();
    this.getSpecialties();
    this.getAllServices();
  }

  getCenters() {
    this.centerService.getCenters().subscribe(
      (response: Center[]) => {
        this.centers = response;
      },
      (error: HttpErrorResponse) => {
        this.errorMessage = 'Entity "Centers" is not available now. Sorry';
        this.handleErrorResponse(error);
      }
    );
  }

  getSpecialties() {
    this.specialtyService.getSpecialties().subscribe(
      (response: Specialty[]) => {
        this.specialties = response;
      },
      (error: HttpErrorResponse) => {
        this.errorMessage = 'Entity "Specialties" is not available now. Sorry';
        this.handleErrorResponse(error);
      }
    );
  }

  getAllServices() {
    this.serviceService.getServices().subscribe(
      (response: Service[]) => {
        this.services = response;
      },
      (error: HttpErrorResponse) => {
        this.errorMessage = 'Entity "Services" is not available now. Sorry.';
        this.handleErrorResponse(error);
      }
    );
  }


  toSpecialtyComponent() {
    this.router.navigate(['/specialties/all']);
  }

  toServiceComponent() {
    this.router.navigate(['/services/all']);
  }

  handleErrorResponse(error: HttpErrorResponse) {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    button.setAttribute('data-target', '#errorModal');
    container.appendChild(button);
    button.click();
  }
}
