import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Review} from "../models/review";
import {FormGroup, NgForm} from "@angular/forms";
import {ReviewDto} from "../models/review.dto";

@Injectable()
export class ReviewService {

  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {
  }

  getReviews() : Observable<Review[]> {
    return this.http.get<Review[]>(`${this.apiServerUrl}/reviews/all`);
  }

  getReviewById(reviewId: number) {
    return this.http.get(`${this.apiServerUrl}/reviews/${reviewId}`);
  }

  getReviewsForUser(): Observable<ReviewDto[]> {
    return this.http.get<ReviewDto[]>(`${this.apiServerUrl}/reviews/all/to/user`);
  }

  createReview(doctorId: number, patientId: number, addForm: FormGroup) {
    return this.http.post(`${this.apiServerUrl}/reviews/create/doctor/${doctorId}/patient/${patientId}`,
      {'reviewBody': addForm.value.review}
      )
  }

  updateReview(reviewId: number, editForm: NgForm) {

    let doctorId = parseInt(editForm.value.doctor);
    let patientId = parseInt(editForm.value.patient);

    return this.http.post(`${this.apiServerUrl}/reviews/update/${reviewId}/doc/${doctorId}/pat/${patientId}`,
      {
        'reviewBody': editForm.value.review
      });
  }

  deleteReview(reviewId: number) {
    return this.http.delete(`${this.apiServerUrl}/reviews/delete/${reviewId}`);
  }
}
