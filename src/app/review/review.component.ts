import {Component, OnInit} from "@angular/core";
import {Review} from "../models/review";
import {ReviewService} from "./review.service";
import {HttpErrorResponse} from "@angular/common/http";
import {ADAService} from "../storage/data-access-service";
import {Doctor} from "../models/doctor";
import {User} from "../models/user";
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {MyErrorHandler} from "../handler/my-error-handler.service";
import {UserService} from "../user/user.service";
import {ReviewDto} from "../models/review.dto";
import {TreatmentService} from "../treatment/treatment.service";
import {DTService} from "../storage/data-transfer-service";
import {Router} from "@angular/router";
import {UserDto} from "../models/user.dto";

@Component({
  selector: 'review-app',
  templateUrl: './review.component.html'
})
export class ReviewComponent implements OnInit{

  page: number = 1;
  reviewForm: FormGroup;

  reviews: Review[] | undefined;
  reviewsDto: ReviewDto[];
  doctors: Doctor[];
  doctorDtoStrings: string[] = [];
  patients: UserDto[];

  user: User;
  role: string;
  patientId: number;

  editReview: Review;
  deleteReview: Review;

  errorOnReview: boolean = false;
  errorMessage: string;


  constructor(private reviewService: ReviewService,
              private adaService: ADAService,
              private userService: UserService,
              private treatmentService: TreatmentService,
              private dtService: DTService,
              private myErrorHandler: MyErrorHandler,
              private router: Router,
              private formBuilder: FormBuilder) {
    this.reviewForm = this.formBuilder.group({
      doctor: [''],
      patient: [''],
      review: ['']
    });
  }

  ngOnInit(): void {

    this.reviewForm = new FormGroup({
      doctor: new FormControl([''], Validators.required),
      patient: new FormControl([''], Validators.required),
      review: new FormControl([''], [Validators.required, Validators.minLength(4)])
    });

    if (localStorage.getItem('username') !== null) {
      this.userService.getUserByUsername(localStorage.getItem('username')).subscribe(
        (res: User) => {
          this.role = res.userRole;
          this.user = res;

          if (res.userRole === 'ROLE_ADMIN') {
            this.getReviews();
            this.getDoctors();
            this.getPatients();
          } else {
            this.getReviewsForUser();
            this.getDoctorsByPatientId();
            this.patchForm();
            this.patientId = this.user.id;
          }
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.role = 'guest';
      this.getReviewsForUser();
    }

  }

  getDoctorsByPatientId() {
    this.treatmentService.getDoctorsByPatientId(this.user.id).subscribe(
      (response: UserDto[]) => {
        this.doDoctorDtoStrings(response);
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  public getReviews(): void {
    this.reviewService.getReviews().subscribe(
      (response: Review[]) => {
        this.reviews = response;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  public getReviewsForUser() {
    this.reviewService.getReviewsForUser().subscribe(
      (response) => {
        this.reviewsDto = response;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  getDoctors() {
    this.adaService.getDoctors().subscribe(
      (response: Doctor[]) => {
        this.doctors = response;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  getPatients() {
    this.userService.getUsersNotDoctors().subscribe(
      (res:UserDto[]) => {
        this.patients = res;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  doSomeOperation(review: Review, operation: string) {

    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if (review !== null) {
      this.reviewService.getReviewById(review.id).subscribe(
        (response: Review) => {
          if (operation === 'edit') {
            this.errorOnReview = false;
            this.editReview = review;
            button.setAttribute('data-target', '#updateReviewModal');
          }

          if (operation === 'delete') {
            this.deleteReview = review;
            button.setAttribute('data-target', '#deleteReviewModal');
          }
          container.appendChild(button);
          button.click();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
          this.getReviews();
        }
      );
    } else {
      if (operation === 'add') {
        this.errorOnReview = false;
        button.setAttribute('data-target', '#addReviewModal');
      }
      if (operation === 'showError') {
        button.setAttribute('data-target', '#errorModal');
      }
      container.appendChild(button);
      button.click();
    }


  }


  onAddReview() {

    if (this.reviewForm.valid) {
      this.errorOnReview = false;
      document.getElementById('add-review-form').click();
      if (this.role === 'ROLE_ADMIN') {
        this.patientId = parseInt(this.reviewForm.value.patient);
      } else {
        this.patientId = this.user.id;
      }
      this.reviewService.createReview(
        parseInt(this.reviewForm.value.doctor),
        this.patientId,
        this.reviewForm
      ).subscribe(
        (res: any) => {
          if (this.role === 'ROLE_ADMIN') {
            this.getReviews();
          } else {
            this.getReviewsForUser();
          }
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );

      /*if (this.role !== 'ROLE_ADMIN') {
        this.router.navigate(['/users/lk']);
      }*/
      this.patchNullValues();
    } else {
      this.errorOnReview = true;
    }
  }


  onUpdateReview(reviewId: number, editForm: NgForm) {

    if (editForm.valid) {
      document.getElementById('edit-review-form').click();
      this.errorOnReview = false;
      this.reviewService.updateReview(reviewId, editForm).subscribe(
        (res: any) => {
          this.getReviews();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.errorOnReview = true;
    }
  }

  onDeleteReview(reviewId: number) {
    this.reviewService.deleteReview(reviewId).subscribe(
      () => {
        this.getReviews();
        this.page = 1;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );


  }

  patchForm() {
    this.reviewForm.patchValue({
      patient: this.user.id
    });

    if (this.dtService.reviewedDoctor !== undefined) {
      let doc = this.dtService.reviewedDoctor;
      this.reviewForm.patchValue({
        doctor: doc.user.id
          + '. ' + doc.user.lastName
          + ' ' + doc.user.firstName
          + ' ' + doc.user.middleName
          + ', ' + doc.specialty.name
      });
    }

    this.doSomeOperation(null, 'add');
  }

  patchNullValues() {
    this.reviewForm.patchValue({
      doctor: null
    });
  }

  handleErrorResponse(error: HttpErrorResponse) {
    this.errorMessage = this.myErrorHandler.handleError(error);
    this.doSomeOperation(null, 'showError');
    this.page = 1;
  }

  doDoctorDtoStrings(doctors: UserDto[]) {
    this.doctorDtoStrings = this.userService.getPersonsStrings(doctors);
  }
}
