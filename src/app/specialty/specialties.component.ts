import {Component, OnInit} from "@angular/core";
import {Specialty} from "../models/specialty";
import {Router} from "@angular/router";
import {SpecialtyService} from "./specialty.service";
import {HttpErrorResponse} from "@angular/common/http";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MyErrorHandler} from "../handler/my-error-handler.service";
import {DTService} from "../storage/data-transfer-service";
import {AuthService} from "../auth/auth.service";
import {UserService} from "../user/user.service";
import {User} from "../models/user";


@Component({
  selector: 'spec-app',
  templateUrl: './specialties.component.html'
})
export class SpecialtiesComponent implements OnInit {

  page: number = 1;

  public specialties: Specialty[] | undefined;

  editSpecialty: Specialty;
  deleteSpecialty: Specialty;
  role: string;

  errorMessage: string;
  errorOnForm: boolean = false;

  specialtyForm: FormGroup;


  constructor( private specialtyService: SpecialtyService,
               private myErrorHandler: MyErrorHandler,
               private dtService: DTService,
               private userService: UserService,
               private authService: AuthService,
               private router: Router,
               private formBuilder: FormBuilder) {
    this.specialtyForm = this.formBuilder.group({
      name: ''
    });
  }

  ngOnInit(): void {

    if (localStorage.getItem('username') !== null) {
      this.userService.getUserByUsername(localStorage.getItem('username')).subscribe(
        (res: User) => {
          this.role = res.userRole;
          this.getSpecialties();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.role = 'guest';
      this.getSpecialties();
    }

    this.specialtyForm = new FormGroup({
      name: new FormControl('',[
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
        Validators.pattern('^[A-Z][a-z-]*$')])
    });
  }

  public getSpecialties(): void {

        this.specialtyService.getSpecialties().subscribe(
          (response: Specialty[]) => {
            this.specialties = response
          },
          (error: HttpErrorResponse) => {
            this.handleErrorResponse(error);
          }
        );
  }

  doSomeOperation(specialty: Specialty, operation: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if (specialty !== null) {
      this.specialtyService.getSpecialtyById(specialty.id).subscribe(
        (response: Specialty) => {
          if (operation === 'edit') {
            this.specialtyForm.reset();
            this.errorOnForm = false;
            this.specialtyForm.patchValue({
              name: specialty.name
            });
            this.editSpecialty = specialty;
            button.setAttribute('data-target', '#updateSpecialtyModal');
          }
          if (operation === 'delete') {
            this.deleteSpecialty = specialty;
            button.setAttribute('data-target', '#deleteSpecialtyModal');
          }
          container.appendChild(button);
          button.click();

        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
          this.getSpecialties();
        }
      );
    } else {
      if (operation === 'add') {
        this.specialtyForm.reset();
        this.errorOnForm = false;
        button.setAttribute('data-target', '#addSpecialtyModal');
      }
      if (operation === 'showError') {
        button.setAttribute('data-target', '#errorModal');
      }
      container.appendChild(button);
      button.click();

    }

  }

  onAddSpecialty(): void {

    if (this.specialtyForm.valid) {
      document.getElementById('add-specialty-form').click();
      this.errorOnForm = false;
      this.specialtyService.createSpecialty(this.specialtyForm.value).subscribe(
        (res:any) => {
          this.getSpecialties();
          this.page = 1;
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.errorOnForm = true;
    }
  }


  onUpdateSpecialty() {

    if (this.specialtyForm.valid) {
      document.getElementById('edit-specialty-form').click();
      this.errorOnForm = false;
      this.specialtyService.updateSpecialty(this.editSpecialty.id, this.specialtyForm).subscribe(
        (res:any) => {
          this.getSpecialties();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      this.errorOnForm = true;
    }

  }

  onDeleteSpecialty(specialtyId: number): void {

    this.specialtyService.deleteSpecialty(specialtyId).subscribe(
      (res:any) => {
        this.getSpecialties();
        this.page = 1;
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  addTreatmentWithSpec(specialtyId: number) {
    if (this.role !== 'guest') {
      this.dtService.changeSpecialtyId(specialtyId);
      this.dtService.specialtyId = specialtyId;
      this.router.navigate(['/treatments/all']);
    }
    else {
      this.router.navigate(['/users/login']);
    }
  }

  handleErrorResponse(error: HttpErrorResponse) {
    this.errorMessage = this.myErrorHandler.handleError(error);
    this.doSomeOperation(null, 'showError');
    this.page = 1;
  }
}
