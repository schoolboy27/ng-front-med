import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Specialty} from "../models/specialty";
import {FormGroup} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class SpecialtyService {

  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) { }

  public getSpecialties(): Observable<Specialty[]> {
    return this.http.get<Specialty[]>(`${this.apiServerUrl}/specialties/all`);
  }

  public getSpecialtyById(specialtyId: number) {
    return this.http.get(`${this.apiServerUrl}/specialties/${specialtyId}`);
  }

  public createSpecialty(specialty: any): Observable<Specialty> {
    return this.http.post<Specialty>(`${this.apiServerUrl}/specialties/create`, specialty);
  }

  public updateSpecialty(specialtyId: number, specialty: FormGroup): Observable<Specialty> {
    return this.http.post<Specialty>(`${this.apiServerUrl}/specialties/update/${specialtyId}`, {
      name: specialty.value.name
    });
  }

  public deleteSpecialty(specialtyId: number): Observable<Specialty> {
    return this.http.delete<Specialty>(`${this.apiServerUrl}/specialties/delete/${specialtyId}`);
  }

}
