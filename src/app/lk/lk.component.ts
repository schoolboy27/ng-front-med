import {Component, OnInit} from "@angular/core";
import {UserService} from "../user/user.service";
import {User} from "../models/user";
import {TreatmentService} from "../treatment/treatment.service";
import {Treatment} from "../models/treatment";
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {Doctor} from "../models/doctor";
import {MmessageService} from "../message/mmessage.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";
import {DTService} from "../storage/data-transfer-service";
import {MyErrorHandler} from "../handler/my-error-handler.service";


@Component({
  selector: 'lk',
  templateUrl: './lk.component.html'
})
export class LKComponent implements OnInit{

  page: number = 1;
  countPerPage: number = 3;
  cDate: Date = new Date();
  currentDate: string;

  editForm: FormGroup;
  editTreatmentForm: FormGroup;

  user: User;
  userMode: boolean = false;
  treatments: Treatment[];
  allTreatments: Treatment[];

  specialtyFilter: string = 'Specialty';
  serviceFilter: string = 'Service';
  specialtiesForFilter: string[] = [];
  servicesForFilter: string[] = [];

  editTreatment: Treatment;
  editDate: string;
  editTime: string;

  deleteTreatment: Treatment;

  doctorFreeTime: string[];

  recipient: User;

  errorMessage: string;
  messageIsEmpty: boolean = false;
  successSending: boolean = false;

  constructor(private userService: UserService,
              private treatmentService: TreatmentService,
              private messageService: MmessageService,
              private dtService: DTService,
              private myErrorHandler: MyErrorHandler,
              private formBuilder: FormBuilder,
              private router: Router) {
    this.editForm = this.formBuilder.group({
      lastName: [''],
      firstName: [''],
      middleName: [''],
      birthDate: [''],
    });

    this.editTreatmentForm = this.formBuilder.group({
      date: [''],
      time: ['']
    });
  }

  ngOnInit(): void {

    this.currentDate = this.cDate.toISOString().substr(0,11) + this.cDate.toLocaleTimeString();

    this.editForm = new FormGroup({
      lastName: new FormControl('', [Validators.required, Validators.pattern('^[A-Z][a-z]*$')]),
      firstName: new FormControl('', [Validators.required, Validators.pattern('^[A-Z][a-z]*$')]),
      middleName: new FormControl('', [Validators.required, Validators.pattern('^[A-Z][a-z]*$')]),
      birthDate: new FormControl('', [Validators.required, Validators.maxLength(10)])
    });

    this.editTreatmentForm = new FormGroup({
      date: new FormControl('', [Validators.required]),
      time: new FormControl('', [Validators.required]),
    });

    if (localStorage.getItem('username') !== null) {
      this.getUser(localStorage.getItem('username'));
    } else {
      this.router.navigate(['/users/login']);
    }

  }

  getUser(username: string) {
    this.userService.getUserByUsername(username).subscribe(
      (response: User) => {
        this.user = response;
        this.getTreatments();
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  doSomeOperation(treatment: Treatment, operation: string) {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if (treatment !== null) {
      this.treatmentService.getTreatmentById(treatment.id).subscribe(
        () => {
          if ((operation === 'editTreatment')) {
            this.editDateTimeTreatment(treatment);
            button.setAttribute('data-target', '#updateDateTimeModal');
          }
          if (operation === 'deleteTreatment') {
            this.deleteTreatment = treatment;
            button.setAttribute('data-target', '#deleteTreatmentModal');
          }
          if (operation === 'writeMessage') {
            if (this.user.userRole === 'ROLE_USER' || this.userMode) {
              this.recipient = treatment.doctor.user;
            } else {
              this.recipient = treatment.patient;
            }
            button.setAttribute('data-target', '#writeMessageModal');
          }

          container.appendChild(button);
          button.click();
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
    } else {
      if ((operation === 'editUser')) {
        this.patchEditForm();
        button.setAttribute('data-target', '#updateUserModal');
      }

      if (operation === 'showError') {
        button.setAttribute('data-target', '#errorModal');
      }

      if (operation === 'sent') {
        button.setAttribute('data-target', '#successModal');
      }
      container.appendChild(button);
      button.click();
    }
  }

  getTreatments() {
    this.serviceFilter = 'Service';
    this.specialtyFilter = 'Specialty';


    if (this.user.id !== undefined) {
      if (this.user.userRole === 'ROLE_USER') {
        this.userMode = true;
        this.getUserTreatments();
      }
      if (this.user.userRole === 'ROLE_DOCTOR') {
        this.page = 1;
        this.userMode = false;
        this.getDoctorTreatments();
      }
      if (this.user.userRole === 'ROLE_ADMIN') {
        this.getAllTreatments();
      }
    }
  }

  getUserTreatments() {
    if (this.user.userRole === 'ROLE_DOCTOR') {
      this.page = 1;
      this.userMode = true;
    }
      this.treatmentService.getTreatmentsByPatientId(this.user.id).subscribe(
        (response: Treatment[]) => {
          this.treatments = response;
          this.allTreatments = response;
          this.getSpecialtiesAndServicesForFilter(response);
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
  }

  getDoctorTreatments() {
      this.treatmentService.getTreatmentsByDoctorId(this.user.id).subscribe(
        (response: Treatment[]) => {
          this.treatments = response;
          this.allTreatments = response;
          this.getSpecialtiesAndServicesForFilter(response);
        },
        (error: HttpErrorResponse) => {
          this.handleErrorResponse(error);
        }
      );
  }

  getAllTreatments() {
    this.treatmentService.getTreatments().subscribe(
      (response: Treatment[]) => {
        this.treatments = response;
        this.allTreatments = response;
        this.getSpecialtiesAndServicesForFilter(response);
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  getSpecialtiesAndServicesForFilter(treatments: Treatment[]) {
    this.getSpecialtiesForFilter(treatments);
    this.getServicesForFilter(treatments);
  }

  getSpecialtiesForFilter(treatments: Treatment[]) {
    treatments.forEach(treatment => {
      this.specialtiesForFilter.push(treatment?.service?.specialty?.name);

    });
    this.specialtiesForFilter = Array.from(new Set(this.specialtiesForFilter));

  }

  getServicesForFilter(treatments: Treatment[]) {
    treatments.forEach(treatment => {
      this.servicesForFilter.push(treatment.service.name);
    });
    this.servicesForFilter = Array.from(new Set(this.servicesForFilter));
  }

  applyFilter() {
    this.page = 1;
    if (this.specialtyFilter !== 'Specialty' || this.serviceFilter !== 'Service') {
      if (this.specialtyFilter !== 'Specialty') {
        this.treatments = this.allTreatments.filter(treatment => treatment.service.specialty.name === this.specialtyFilter);
      } else {
        this.treatments = this.allTreatments.filter(treatment => treatment.service.name === this.serviceFilter);
      }
      if (this.serviceFilter !== 'Service') {
        this.treatments = this.treatments.filter(treatment => treatment.service.name === this.serviceFilter);
      }
    } else {
      this.treatments = this.allTreatments;
    }
  }

  onUpdateTreatment(value: any) {
    let appointment = value.date + 'T' + value.time;
    let oldAppointment = this.editDate + 'T' + this.editTime;

    this.treatmentService.updateDoctorSchedule(
      this.editTreatment.doctor.user.id,
      this.editTreatment.patient.id,
      appointment,
      oldAppointment
      ).subscribe(
      () => {
        this.treatmentService.updateTreatment(
          this.editTreatment.id,
          this.editTreatment.service.id,
          this.editTreatment.doctor.user.id,
          this.editTreatment.patient.id,
          value.date + 'T' + value.time
        ).subscribe(
          (res) => {
            this.resetFilter();
          },
          (error: HttpErrorResponse) => {
            this.handleErrorResponse(error);
          }
        );
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  editDateTimeTreatment(treatment: Treatment) {
    this.retrieveDateAndTime(treatment);
    this.editTreatment = treatment;

    this.getDoctorFreeTime(treatment.doctor.user.id, this.editDate);
    this.patchEditTreatmentForm();
  }

  getDoctorFreeTime(doctorId: number, date: string) {

    this.treatmentService.getDoctorFreeTime(doctorId, date).subscribe(
      (response: string[]) => {
        this.doctorFreeTime = response;
        this.doctorFreeTime.push(this.editTime);
        this.doctorFreeTime.sort();
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  retrieveDateAndTime(treatment: Treatment) {
    this.editDate = treatment.appointment.toString().substr(0, 10);
    this.editTime = treatment.appointment.toString().substr(11, 8);
  }

  patchEditForm() {
    this.editForm.patchValue({
      lastName: this.user.lastName,
      firstName: this.user.firstName,
      middleName: this.user.middleName,
      birthDate: this.user.birthDate
    });

  }

  patchEditTreatmentForm() {
    this.editTreatmentForm.patchValue({
      date: this.editDate,
      time: this.editTime
    })
  }

  onUpdateUser(form: FormGroup) {
    let agePass = this.userService.checkAge(form.value.birthDate);

    if (form.value.birthDate.length !== 10 || !agePass) {
      form.patchValue({
        birthDate: null
      });
    }
    this.userService.updateUser(this.user.id, form.value).subscribe(
      (res) => {
        console.log(res);
        this.getUser(this.user.login);
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );
  }

  onDeleteTreatment(treatmentId: number) {

    this.treatmentService.updateDoctorSchedule(
      this.deleteTreatment?.doctor.user.id,
      -1,
      this.deleteTreatment.appointment.toString(),
      this.deleteTreatment.appointment.toString()).subscribe(
      () => {
        this.treatmentService.deleteTreatment(treatmentId).subscribe(
          () => {
            if (this.userMode) {
              this.getUserTreatments();
            } else {
              this.getTreatments();
            }
            this.resetFilter();
          },
          (error: HttpErrorResponse) => {
            this.handleErrorResponse(error);
          }
        );
      },
      (error: HttpErrorResponse) => {
        this.handleErrorResponse(error);
      }
    );

  }


  sendMessage(messageForm: NgForm) {

    this.messageService.createMessage(this.user.id, this.recipient.id, messageForm).subscribe(
      (res) => {
        document.getElementById('send-form').click();
        this.doSomeOperation(null, 'sent');
      },
      (error: HttpErrorResponse) => {
        this.successSending = false;
        if ((error.status === 400) || (error.status === 500)) {
          this.messageIsEmpty = true;
        } else {
          this.handleErrorResponse(error);
        }
      }
    );
  }

  getUserMessages() {
    this.router.navigate(['/users/chat']);
  }

  addTreatment() {
    this.router.navigate(['/treatments/all']);
  }

  giveReview(doctor: Doctor) {
    this.dtService.reviewedDoctor = doctor;
    this.router.navigate(['/reviews/all']);
  }


  resetFilter() {
    this.specialtyFilter = 'Specialty';
    this.serviceFilter = 'Service';
    if (this.userMode) {
      this.getUserTreatments();
    } else {
      this.getTreatments();
    }
    this.page = 1;
  }

  handleErrorResponse(error: HttpErrorResponse) {
    this.errorMessage = this.myErrorHandler.handleError(error);
    this.doSomeOperation(null, 'showError');
  }
}
