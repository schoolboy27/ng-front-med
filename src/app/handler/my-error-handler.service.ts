import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";


@Injectable()
export class MyErrorHandler {

  errorMessage: string = '';

  constructor(private router: Router) {
  }

  handleError(error: HttpErrorResponse): string {
    if (error.status === 500) {
      return this.handle500Error(error);
    }
    else if (error.status === 401) {
      return this.handle401Error(error);
    }
    else if (error.status === 404) {
      return this.handle404Error(error);
    }
    else if (error.status === 403) {
      return this.handle403Error(error);
    }
    else if (error.status === 400) {
      return this.handle400Error(error);
    }
    else {
      return this.handleOtherError(error);
    }

  }

  handle500Error(error: HttpErrorResponse) {
    return 'This resource is no longer available. Or this resource is linked with other resource and cannot be deleted.';
  }

  handle404Error(error: HttpErrorResponse) {
    return 'This object is not found';
  }

  handle403Error(error: HttpErrorResponse) {
    this.router.navigate(['/403']);
    return 'Unauthorized request';
  }

  handle401Error(error: HttpErrorResponse) {
    window.location.reload();
    return 'Token is expired';
  }

  handle400Error(error: HttpErrorResponse) {
    return 'Bad request. Some params are invalid.';
  }

  handleOtherError(error: HttpErrorResponse) {
    this.router.navigate(['/']);
    return 'Other error';
  }

  createErrorMessage(error: HttpErrorResponse) {
    this.errorMessage = error.error ? error.error : error.statusText;
    console.log(this.errorMessage);
  }

}
